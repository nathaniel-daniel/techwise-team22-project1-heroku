from flask import Flask, request, render_template, jsonify
import pickle

from sklearn.feature_extraction.text import HashingVectorizer, CountVectorizer
from sklearn.naive_bayes import BernoulliNB

import nltk
from nltk.tokenize import word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer

import numpy

class LemmaTokenizer:
    def __init__(self):
        self.wnl = WordNetLemmatizer()
    def __call__(self, doc):
        return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]
        
class Unpickler(pickle.Unpickler):
    def find_class(self, module, name):
        match (module, name):
            case ('sklearn.feature_extraction.text', 'HashingVectorizer'):
                return HashingVectorizer
            case ('sklearn.feature_extraction.text', 'CountVectorizer'):
                return CountVectorizer
            case ('nltk.stem.wordnet', 'WordNetLemmatizer'):
                return WordNetLemmatizer
            case ('sklearn.naive_bayes', 'BernoulliNB'):
                return BernoulliNB
            case ('numpy.core.multiarray', '_reconstruct'):
                return getattr(numpy.core.multiarray, '_reconstruct')
            case ('numpy', _):
                return getattr(numpy, name)
            case (_, 'LemmaTokenizer'):
                return LemmaTokenizer
            case (_, _):
                raise pickle.UnpicklingError(f"unknown global (module={module}, name={name})")

app = Flask(__name__)

with open('vectorizer.pkl', 'rb') as f:
    vectorizer = Unpickler(f).load()
with open('model.pkl', 'rb') as f:
    classifier = Unpickler(f).load()

@app.route('/', methods=['GET'])
def index():    
    return render_template("model-html.html")

@app.route('/api/classify', methods=['POST'])
def api_classify():
    X = request.json['text']
    X = vectorizer.transform([X])
    
    
    proba = classifier.predict_proba(X)[0]
    proba_nta = proba[0]
    proba_yta = proba[1]
    
    prediction = classifier.predict(X)
    prediction = 'YTA' if prediction[0] else 'NTA'
    
    return jsonify({
        'classification': prediction,
        'probability': {
            'NTA': proba_nta,
            'YTA': proba_yta,
        }
    })
    
if __name__ == '__main__':
    app.run(debug = True)